import { TestBed, inject } from '@angular/core/testing';

import { AppService } from './app.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { HttpErrorHandler } from './http-error-handler.service';

describe('AppService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, BrowserDynamicTestingModule],
      providers: [
        AppService,
        {
          provide: HttpErrorHandler,
          useValue: {
            createHandleError: (...args) => {},
          },
        },
      ],
    });
  });

  it('should be created', inject([AppService], (service: AppService) => {
    expect(service).toBeTruthy();
  }));
});
