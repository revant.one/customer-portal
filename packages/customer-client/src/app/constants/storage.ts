export const ORGANIZATION_TYPES = [
  'Private Limited',
  'LLP',
  'General Partnership',
  'Incorporated',
  'Startup',
  'Individual',
];
export const PRIORITY_TYPE = ['Low', 'Medium', 'High', 'Critical'];
export const BINARY = ['Yes', 'No'];
export const QUETY_TYPE = [
  'Consultancy',
  'Collaboration',
  'Discussion',
  'Software Development',
  'Technical Support',
];
export const CLOSE = 'Close';
