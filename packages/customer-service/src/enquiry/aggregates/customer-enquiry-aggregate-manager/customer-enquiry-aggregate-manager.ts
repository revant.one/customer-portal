import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { Enquiry } from '../../entities/customer-enquiry/customer-enquiry.collection';
import { CustomerEnquiryCreatedEvent } from '../../events/customer-enquiry-created/customer-enquiry-created.event';

export class CustomerEnquiryAggregateManager extends AggregateRoot {
  constructor() {
    super();
  }

  createNewCustomerInquiry(clientFormRequest) {
    clientFormRequest.uuid = uuidv4();
    const provider = Object.assign(new Enquiry(), clientFormRequest);
    return this.apply(new CustomerEnquiryCreatedEvent(provider));
  }
}
