import { Test, TestingModule } from '@nestjs/testing';
import { CustomerEnquiryController } from './customer-enquiry.controller';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../guards/token.guard';
import { SettingsService } from '../../../models/settings/settings.service';
import { TokenCacheService } from '../../../models/token-cache/token-cache.service';
import { HttpService } from '@nestjs/common';

describe('Customer Enquiry Controller', () => {
  let controller: CustomerEnquiryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CustomerEnquiryController],
      providers: [
        {
          provide: CommandBus,
          useValue: {},
        },
        {
          provide: QueryBus,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();

    controller = module.get<CustomerEnquiryController>(
      CustomerEnquiryController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
