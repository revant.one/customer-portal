import { Global, Module, HttpModule } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CustomerEnquiryController } from './controllers/customer-enquiry/customer-enquiry.controller';
import { EnquiryEntityProvider } from './entities';
import { CustomerManagerAggregates } from './aggregates';
import { EnquiryEventHandlers } from './events';
import { EnquiryCommandHandlers } from './commands';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Enquiry } from './entities/customer-enquiry/customer-enquiry.collection';
import { QueriesHandlers } from './queries';
import { TokenGuard } from '../guards/token.guard';
import { RoleGuard } from '../guards/role.guard';

Global();
@Module({
  imports: [TypeOrmModule.forFeature([Enquiry]), CqrsModule, HttpModule],
  providers: [
    TokenGuard,
    RoleGuard,
    ...EnquiryEntityProvider,
    ...CustomerManagerAggregates,
    ...EnquiryCommandHandlers,
    ...EnquiryEventHandlers,
    ...QueriesHandlers,
  ],
  controllers: [CustomerEnquiryController],
})
export class EnquiryModule {}
