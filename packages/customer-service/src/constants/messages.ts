export const SETTINGS_ALREADY_EXISTS = 'Settings already exists';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const USER_ALREADY_EXIST = 'User already exists';
