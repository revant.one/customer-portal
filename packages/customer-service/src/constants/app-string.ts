export const SEND_EMAIL = 'send_email';
export const AUTHORIZATION = 'authorization';
export const ADMINISTRATOR = 'administrator';
export const CUSTOMER_RELATION_MANAGER = 'customer-relation-manager';
export const TOKEN = 'token';
export const SERVICE_DOWN = 'Connected service is down';
export const CUSTOMER_SERVICE = 'customer-service';
