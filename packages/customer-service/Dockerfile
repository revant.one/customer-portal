FROM node:latest
# Copy and build server
COPY packages/customer-service /home/craft/customer-service
WORKDIR /home/craft/
RUN cd customer-service \
    && rm -fr .env \
    && npm install \
    && rm -fr dist \
    && npm run build \
    && rm -fr node_modules \
    && npm install --only=production

FROM node:slim
# Install Dockerize
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Setup docker-entrypoint
COPY packages/customer-service/docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

# Add non root user and set home directory
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/customer-service
COPY --from=0 /home/craft/customer-service .

# Copy Docker files and config
COPY packages/customer-service/docker ./docker/
RUN chown -R craft:craft /home/craft

# Expose port
EXPOSE 5800

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
